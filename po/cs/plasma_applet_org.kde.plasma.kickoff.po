# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# SPDX-FileCopyrightText: 2013, 2014, 2015, 2016, 2017, 2019, 2020, 2023 Vít Pelčák <vit@pelcak.org>
# Vit Pelcak <vit@pelcak.org>, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-27 01:41+0000\n"
"PO-Revision-Date: 2023-11-10 16:31+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.2\n"
"X-Language: cs_CZ\n"
"X-Source-Language: en_US\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Obecné"

#: package/contents/ui/code/tools.js:49
#, kde-format
msgid "Remove from Favorites"
msgstr "Odstranit z oblíbených"

#: package/contents/ui/code/tools.js:53
#, kde-format
msgid "Add to Favorites"
msgstr "Přidat do oblíbených"

#: package/contents/ui/code/tools.js:77
#, kde-format
msgid "On All Activities"
msgstr "Ve všech aktivitách"

#: package/contents/ui/code/tools.js:127
#, kde-format
msgid "On the Current Activity"
msgstr "V současné aktivitě"

#: package/contents/ui/code/tools.js:141
#, kde-format
msgid "Show in Favorites"
msgstr "Zobrazit v oblíbených"

#: package/contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Icon:"
msgstr "Ikona:"

#: package/contents/ui/ConfigGeneral.qml:47
#, kde-format
msgctxt "@action:button"
msgid "Change Application Launcher's icon"
msgstr "Změnit ikonu spouštěče aplikace"

#: package/contents/ui/ConfigGeneral.qml:48
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""
"Současná ikona je %1. Klikněte pro otevření nabídky pro změnu současné ikony "
"nebo vrácení původní ikony."

#: package/contents/ui/ConfigGeneral.qml:52
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr "Název ikony je \"%1\""

#: package/contents/ui/ConfigGeneral.qml:85
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Vybrat…"

#: package/contents/ui/ConfigGeneral.qml:87
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Application Launcher"
msgstr "Vyberte ikonu spouštěče aplikace"

#: package/contents/ui/ConfigGeneral.qml:91
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr "Obnovit výchozí ikonu"

#: package/contents/ui/ConfigGeneral.qml:97
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr "Odstranit ikonu"

#: package/contents/ui/ConfigGeneral.qml:108
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr "Textový popisek:"

#: package/contents/ui/ConfigGeneral.qml:110
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr "Pro přidání textového popisku pište zde"

#: package/contents/ui/ConfigGeneral.qml:125
#, kde-format
msgctxt "@action:button"
msgid "Reset menu label"
msgstr "Resetovat popisek nabídky"

#: package/contents/ui/ConfigGeneral.qml:139
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr "Pokud je panel svislý, nelze nastavit textový popisek."

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgctxt "General options"
msgid "General:"
msgstr "Obecné:"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Always sort applications alphabetically"
msgstr "Vždy řadit aplikace podle abecedy"

#: package/contents/ui/ConfigGeneral.qml:156
#, kde-format
msgid "Use compact list item style"
msgstr "Použít kompaktní styl seznamu položek"

#: package/contents/ui/ConfigGeneral.qml:162
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr "Automaticky zakázáno v dotykovém režimu"

#: package/contents/ui/ConfigGeneral.qml:171
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Nastavit povolené moduly pro vyhledávání…"

#: package/contents/ui/ConfigGeneral.qml:181
#, kde-format
msgid "Sidebar position:"
msgstr "Pozice postranní lišty:"

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Right"
msgstr "Vpravo"

#: package/contents/ui/ConfigGeneral.qml:182
#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Left"
msgstr "Vlevo"

#: package/contents/ui/ConfigGeneral.qml:198
#, kde-format
msgid "Show favorites:"
msgstr "Zobrazit oblíbené:"

#: package/contents/ui/ConfigGeneral.qml:199
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr "V mřížce"

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr "V seznamu"

#: package/contents/ui/ConfigGeneral.qml:215
#, kde-format
msgid "Show other applications:"
msgstr "Zobrazit ostatní aplikace:"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr "V mřížce"

#: package/contents/ui/ConfigGeneral.qml:224
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr "V seznamu"

#: package/contents/ui/ConfigGeneral.qml:236
#, kde-format
msgid "Show buttons for:"
msgstr "Zobrazit tlačítka pro:"

#: package/contents/ui/ConfigGeneral.qml:237
#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Power"
msgstr "Napájení"

#: package/contents/ui/ConfigGeneral.qml:246
#, kde-format
msgid "Session"
msgstr "Sezení"

#: package/contents/ui/ConfigGeneral.qml:255
#, kde-format
msgid "Power and session"
msgstr "Napájení a sezení"

#: package/contents/ui/ConfigGeneral.qml:264
#, kde-format
msgid "Show action button captions"
msgstr "Zobrazit popisky akčních tlačítek"

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "Aplikace"

#: package/contents/ui/Footer.qml:113
#, kde-format
msgid "Places"
msgstr "Místa"

#: package/contents/ui/FullRepresentation.qml:153
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr "Žádné shody"

#: package/contents/ui/Header.qml:80
#, kde-format
msgid "Open user settings"
msgstr "Otevřít nastavení uživatele"

#: package/contents/ui/Header.qml:257
#, kde-format
msgid "Keep Open"
msgstr "Ponechat otevřené"

#: package/contents/ui/KickoffGridView.qml:92
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr "Mřížka s %1 řádky, %2 sloupci"

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "Leave"
msgstr "Opustit"

#: package/contents/ui/LeaveButtons.qml:129
#, kde-format
msgid "More"
msgstr "Více"

#: package/contents/ui/main.qml:316
#, kde-format
msgid "Edit Applications…"
msgstr "Upravit aplikace…"

#: package/contents/ui/PlacesPage.qml:48
#, kde-format
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "Počítač"

#: package/contents/ui/PlacesPage.qml:49
#, kde-format
msgctxt "category in Places sidebar"
msgid "History"
msgstr "Historie"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr "Často používané"
