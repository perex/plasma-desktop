# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2017.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019, 2020, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-08 01:39+0000\n"
"PO-Revision-Date: 2023-08-26 04:45+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: actions.cpp:19
#, kde-format
msgid "Touchpad"
msgstr "Área táctil"

#: actions.cpp:22
#, kde-format
msgid "Enable Touchpad"
msgstr "Activar a área táctil"

#: actions.cpp:30
#, kde-format
msgid "Disable Touchpad"
msgstr "Desactivar a área táctil"

#: actions.cpp:38
#, kde-format
msgid "Toggle Touchpad"
msgstr "Conmutar a área táctil"

#: backends/kwin_wayland/kwinwaylandbackend.cpp:59
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""
"A consulta dos dispositivos de entrada fallou. Abra de novo o módulo de "
"configuración."

#: backends/kwin_wayland/kwinwaylandbackend.cpp:74
#, kde-format
msgid "Critical error on reading fundamental device infos for touchpad %1."
msgstr ""
"Produciuse un erro crítico ao ler información de dispositivo fundamental da "
"área táctil %1."

#: backends/x11/xlibbackend.cpp:71
#, kde-format
msgid "Cannot connect to X server"
msgstr "Non é posíbel conectar ao servidor das X."

#: backends/x11/xlibbackend.cpp:84 kcm/libinput/touchpad.qml:94
#, kde-format
msgid "No touchpad found"
msgstr "Non se atopou ningún touchpad."

#: backends/x11/xlibbackend.cpp:124 backends/x11/xlibbackend.cpp:138
#, kde-format
msgid "Cannot apply touchpad configuration"
msgstr "Non é posíbel aplicar a configuración da área táctil."

#: backends/x11/xlibbackend.cpp:152 backends/x11/xlibbackend.cpp:165
#, kde-format
msgid "Cannot read touchpad configuration"
msgstr "Non é posíbel obter a configuración da área táctil."

#: backends/x11/xlibbackend.cpp:178
#, kde-format
msgid "Cannot read default touchpad configuration"
msgstr "Non é posíbel ler a configuración predeterminada da área táctil"

#: kcm/libinput/touchpad.qml:108
#, kde-format
msgid "Device:"
msgstr "Dispositivo:"

#: kcm/libinput/touchpad.qml:134
#, kde-format
msgid "General:"
msgstr "Xeral:"

#: kcm/libinput/touchpad.qml:135
#, kde-format
msgid "Device enabled"
msgstr "Dispositivo activado"

#: kcm/libinput/touchpad.qml:139
#, kde-format
msgid "Accept input through this device."
msgstr "Aceptar entrada deste dispositivo."

#: kcm/libinput/touchpad.qml:163
#, kde-format
msgid "Disable while typing"
msgstr "Desactivar mentres se teclea"

#: kcm/libinput/touchpad.qml:167
#, kde-format
msgid "Disable touchpad while typing to prevent accidental inputs."
msgstr ""
"Desactivar a área táctil mentres se teclea para evitar tocala por accidente"

#: kcm/libinput/touchpad.qml:191
#, kde-format
msgid "Left handed mode"
msgstr "Modo para zurdos"

#: kcm/libinput/touchpad.qml:195
#, kde-format
msgid "Swap left and right buttons."
msgstr "Intercambiar os botóns esquerdo e dereito."

#: kcm/libinput/touchpad.qml:219
#, kde-format
msgid "Press left and right buttons for middle click"
msgstr "Prema os botóns esquerdo e dereito para facer un clic central"

#: kcm/libinput/touchpad.qml:223 kcm/libinput/touchpad.qml:864
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""
"Premer os botóns esquerdo e dereito á vez envía un clic do botón central."

#: kcm/libinput/touchpad.qml:254
#, kde-format
msgid "Pointer speed:"
msgstr "Velocidade do punteiro:"

#: kcm/libinput/touchpad.qml:352
#, kde-format
msgid "Pointer acceleration:"
msgstr "Aceleración do punteiro:"

#: kcm/libinput/touchpad.qml:383
#, kde-format
msgid "None"
msgstr "Ningún"

#: kcm/libinput/touchpad.qml:387
#, kde-format
msgid "Cursor moves the same distance as finger."
msgstr "O cursor móvese a mesma distancia que o dedo."

#: kcm/libinput/touchpad.qml:396
#, kde-format
msgid "Standard"
msgstr "Estándar"

#: kcm/libinput/touchpad.qml:400
#, kde-format
msgid "Cursor travel distance depends on movement speed of finger."
msgstr ""
"A distancia de movemento do cursor depende da velocidade de movemento do "
"dedo."

#: kcm/libinput/touchpad.qml:415
#, kde-format
msgid "Tapping:"
msgstr "Toques:"

#: kcm/libinput/touchpad.qml:416
#, kde-format
msgid "Tap-to-click"
msgstr "Tocar para premer"

#: kcm/libinput/touchpad.qml:420
#, kde-format
msgid "Single tap is left button click."
msgstr "Un toque é un clic do botón esquerdo."

#: kcm/libinput/touchpad.qml:449
#, kde-format
msgid "Tap-and-drag"
msgstr "Tocar e arrastrar"

#: kcm/libinput/touchpad.qml:453
#, kde-format
msgid "Sliding over touchpad directly after tap drags."
msgstr ""
"Arrastrando pola área táctil directamente despois de arrastres de toque."

#: kcm/libinput/touchpad.qml:480
#, kde-format
msgid "Tap-and-drag lock"
msgstr "Bloqueo de tocar e arrastrar"

#: kcm/libinput/touchpad.qml:484
#, kde-format
msgid "Dragging continues after a short finger lift."
msgstr "O arrastre continúa tras levantar brevemente o dedo."

#: kcm/libinput/touchpad.qml:504
#, kde-format
msgid "Two-finger tap:"
msgstr "Toque con dous dedos:"

#: kcm/libinput/touchpad.qml:515
#, kde-format
msgid "Right-click (three-finger tap to middle-click)"
msgstr "Clic dereito (toque con tres dedos para clic central)"

#: kcm/libinput/touchpad.qml:516
#, kde-format
msgid ""
"Tap with two fingers to right-click, tap with three fingers to middle-click."
msgstr ""
"Tocar con dous dedos para facer clic dereito, tocar con tres dedos para "
"facer clic central."

#: kcm/libinput/touchpad.qml:518
#, kde-format
msgid "Middle-click (three-finger tap right-click)"
msgstr "Clic central (toque con tres dedos para clic dereito)"

#: kcm/libinput/touchpad.qml:519
#, kde-format
msgid ""
"Tap with two fingers to middle-click, tap with three fingers to right-click."
msgstr ""
"Tocar con dous dedos para facer clic central, tocar con tres dedos para "
"facer clic dereito."

#: kcm/libinput/touchpad.qml:521
#, kde-format
msgid "Right-click"
msgstr "Clic dereito"

#: kcm/libinput/touchpad.qml:522
#, kde-format
msgid "Tap with two fingers to right-click."
msgstr "Tocar con dous dedos para facer clic dereito."

#: kcm/libinput/touchpad.qml:524
#, kde-format
msgid "Middle-click"
msgstr "Clic central"

#: kcm/libinput/touchpad.qml:525
#, kde-format
msgid "Tap with two fingers to middle-click."
msgstr "Tocar con dous dedos para facer clic central."

#: kcm/libinput/touchpad.qml:584
#, kde-format
msgid "Scrolling:"
msgstr "Desprazamento:"

#: kcm/libinput/touchpad.qml:613
#, kde-format
msgid "Two fingers"
msgstr "Dous dedos"

#: kcm/libinput/touchpad.qml:617
#, kde-format
msgid "Slide with two fingers scrolls."
msgstr "Arrastrar con dous dedos despraza."

#: kcm/libinput/touchpad.qml:625
#, kde-format
msgid "Touchpad edges"
msgstr "Bordos da área táctil"

#: kcm/libinput/touchpad.qml:629
#, kde-format
msgid "Slide on the touchpad edges scrolls."
msgstr "Arrastrar nos bordos da área táctil despraza."

#: kcm/libinput/touchpad.qml:639
#, kde-format
msgid "Invert scroll direction (Natural scrolling)"
msgstr "Inverter a dirección de desprazamento (desprazamento natural)"

#: kcm/libinput/touchpad.qml:655
#, kde-format
msgid "Touchscreen like scrolling."
msgstr "Desprazamento como o dunha pantalla táctil."

#: kcm/libinput/touchpad.qml:663 kcm/libinput/touchpad.qml:680
#, kde-format
msgid "Disable horizontal scrolling"
msgstr "Desactivar o desprazamento horizontal"

#: kcm/libinput/touchpad.qml:688
#, kde-format
msgid "Scrolling speed:"
msgstr "Velocidade de desprazamento:"

#: kcm/libinput/touchpad.qml:739
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "Máis amodo"

#: kcm/libinput/touchpad.qml:745
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "Veloz"

#: kcm/libinput/touchpad.qml:755
#, kde-format
msgid "Right-click:"
msgstr "Clic dereito:"

#: kcm/libinput/touchpad.qml:789
#, kde-format
msgid "Press bottom-right corner"
msgstr "Premer a esquina inferior dereita"

#: kcm/libinput/touchpad.qml:793
#, kde-format
msgid ""
"Software enabled buttons will be added to bottom portion of your touchpad."
msgstr ""
"Os botóns activados por software engadiranse á parte inferior da zona táctil."

#: kcm/libinput/touchpad.qml:801
#, kde-format
msgid "Press anywhere with two fingers"
msgstr "Prema en calquera lugar con dous dedos"

#: kcm/libinput/touchpad.qml:805
#, kde-format
msgid "Tap with two finger to enable right click."
msgstr "Tocar con dous dedos para activar o clic dereito."

#: kcm/libinput/touchpad.qml:819
#, kde-format
msgid "Middle-click: "
msgstr "Clic central: "

#: kcm/libinput/touchpad.qml:848
#, kde-format
msgid "Press bottom-middle"
msgstr "Premer o inferior medio"

#: kcm/libinput/touchpad.qml:852
#, kde-format
msgid ""
"Software enabled middle-button will be added to bottom portion of your "
"touchpad."
msgstr ""
"O botón central activado por software engadirase á parte inferior da zona "
"táctil."

#: kcm/libinput/touchpad.qml:860
#, kde-format
msgid "Press bottom left and bottom right corners simultaneously"
msgstr "Prema as esquinas inferior esquerda e inferior dereita simultaneamente"

#: kcm/libinput/touchpad.qml:873
#, kde-format
msgid "Press anywhere with three fingers"
msgstr "Prema en calquera lugar con tres dedos"

#: kcm/libinput/touchpad.qml:879
#, kde-format
msgid "Press anywhere with three fingers."
msgstr "Prema en calquera lugar con tres dedos."

#: kcm/touchpadconfig.cpp:99
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""
"Erro ao cargar os valores. Consulte o rexistro para máis información. "
"Reinicie este módulo de configuración."

#: kcm/touchpadconfig.cpp:102
#, kde-format
msgid "No touchpad found. Connect touchpad now."
msgstr "Non se atopou ningunha área táctil."

#: kcm/touchpadconfig.cpp:111
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""
"Non foi posíbel gardar todos os cambios. Consulte o rexistro para máis "
"información. Reinicie o módulo de configuración e inténteo de novo."

#: kcm/touchpadconfig.cpp:130
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""
"Erro ao cargar os valores predeterminados. Non se puido asignarlle a "
"algunhas opcións os seus valores predeterminados."

#: kcm/touchpadconfig.cpp:150
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""
"Erro ao engadir o dispositivo que acababa de conectar. Conécteo de novo e "
"reinicie o módulo de configuración."

#: kcm/touchpadconfig.cpp:173
#, kde-format
msgid "Touchpad disconnected. Closed its setting dialog."
msgstr "Desconectouse a área táctil. Pechouse o seu diálogo de configuración."

#: kcm/touchpadconfig.cpp:175
#, kde-format
msgid "Touchpad disconnected. No other touchpads found."
msgstr "Desconectouse a área táctil. Non se atopou ningunha outra área táctil."

#: kded/kded.cpp:201
#, kde-format
msgid "Touchpad was disabled because a mouse was plugged in"
msgstr "Desactivouse a área táctil porque se conectou un rato."

#: kded/kded.cpp:204
#, kde-format
msgid "Touchpad was enabled because the mouse was unplugged"
msgstr "Activouse a área táctil porque se desconectou o rato."

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:27
#, kde-format
msgctxt "Emulated mouse button"
msgid "No action"
msgstr "Ningunha acción"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:30
#, kde-format
msgctxt "Emulated mouse button"
msgid "Left button"
msgstr "Botón esquerdo"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:33
#, kde-format
msgctxt "Emulated mouse button"
msgid "Middle button"
msgstr "Botón central"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:36
#, kde-format
msgctxt "Emulated mouse button"
msgid "Right button"
msgstr "Botón dereito"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:285
#, kde-format
msgctxt "Touchpad Edge"
msgid "All edges"
msgstr "Todas as esquinas"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:288
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top edge"
msgstr "Bordo superior"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:291
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top right corner"
msgstr "Esquina superior dereita"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:294
#, kde-format
msgctxt "Touchpad Edge"
msgid "Right edge"
msgstr "Bordo dereito"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:297
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom right corner"
msgstr "Esquina inferior dereita"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:300
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom edge"
msgstr "Bordo inferior"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:303
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom left corner"
msgstr "Esquina inferior esquerda"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:306
#, kde-format
msgctxt "Touchpad Edge"
msgid "Left edge"
msgstr "Bordo esquerdo"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:309
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top left corner"
msgstr "Esquina superior esquerda"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Adrian Chaves (Gallaecio)"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "adrian@chaves.io"

#~ msgid "Drag me"
#~ msgstr "Arrástreme"

#~ msgctxt "Mouse button"
#~ msgid "Left button"
#~ msgstr "Botón esquerdo"

#~ msgctxt "Mouse button"
#~ msgid "Right button"
#~ msgstr "Botón dereito"

#~ msgctxt "Mouse button"
#~ msgid "Middle button"
#~ msgstr "Botón central"

#~ msgid ""
#~ "Active settings don't match saved settings.\n"
#~ "You currently see saved settings."
#~ msgstr ""
#~ "A configuración activa non se corresponde coa gardada.\n"
#~ "A configuración que está a ver é a gardada."

#~ msgid "Show active settings"
#~ msgstr "Amosar a configuración activa"

#~ msgid "These settings won't take effect in the testing area"
#~ msgstr "Esta configuración non afectará á zona de probas."

#~ msgid "Enable/Disable Touchpad"
#~ msgstr "Activar ou desactivar a área táctil"

#~ msgctxt "@title:window"
#~ msgid "Enable/Disable Touchpad"
#~ msgstr "Activar ou desactivar a área táctil"

#~ msgid "Configure…"
#~ msgstr "Configurar…"

#~ msgid "Keyboard shortcuts:"
#~ msgstr "Atallos de teclado:"

#~ msgid "Disable touchpad when typing"
#~ msgstr "Desactivar a área táctil mentres se teclea."

#~ msgid "Timeout:"
#~ msgstr "Tempo límite:"

#~ msgid ""
#~ "If there was no keyboard activity for more than specified\n"
#~ "number of milliseconds, touchpad will be enabled again"
#~ msgstr ""
#~ "Se pasaron máis milisegundos dos indicados sen\n"
#~ "actividade no teclado, activar de novo a área táctil."

#~ msgid " ms"
#~ msgstr " ms"

#~ msgid ""
#~ "Disable click emulation and scrolling.\n"
#~ "You will be still able to move pointer using touchpad and perform clicks "
#~ "with hardware buttons."
#~ msgstr ""
#~ "Desactivar a emulación de clics e de desprazamento.\n"
#~ "Aínda poderá mover o punteiro mediante a área táctil e facer clic cos "
#~ "botóns de soporte físico."

#~ msgid "Disable taps and scrolling only"
#~ msgstr "Só desactivar os toques e o desprazamento."

#~ msgid "Disable touchpad when mouse is plugged in"
#~ msgstr "Desactivar a área táctil ao conectar un rato."

#~ msgid "Notifications:"
#~ msgstr "Notificacións:"

#~ msgid "Ignored devices:"
#~ msgstr "Dispositivos ignorados:"

#~ msgid ""
#~ "Some devices might be incorrectly treated as a mouse.\n"
#~ "You can add them to this list so that they will be ignored when they're "
#~ "plugged in."
#~ msgstr ""
#~ "Algúns dispositivos poderían interpretarse erroneamente como ratos.\n"
#~ "Pode engadilos a esta lista para que se ignoren."

#~ msgid "Pointer Motion"
#~ msgstr "Movemento do punteiro"

#~ msgctxt "Mouse pointer motion parameters"
#~ msgid "Speed"
#~ msgstr "Velocidade"

#~ msgctxt "Minimum pointer movement speed"
#~ msgid "Minimum:"
#~ msgstr "Mínima:"

#~ msgid "Minimum pointer speed"
#~ msgstr "velocidade mínima do punteiro."

#~ msgctxt "Maximum pointer movement speed"
#~ msgid "Maximum:"
#~ msgstr "Máxima:"

#~ msgid "Maximum pointer speed"
#~ msgstr "Velocidade máxima do punteiro."

#~ msgctxt "Pointer acceleration"
#~ msgid "Acceleration:"
#~ msgstr "Aceleración:"

#~ msgid "Acceleration factor for normal pointer movements"
#~ msgstr "Factor de aceleración para os movementos normais do punteiro."

#~ msgctxt "Pointer motion"
#~ msgid "Pressure-Dependent Motion"
#~ msgstr "Movemento segundo a presión"

#~ msgctxt "Pressure-dependent pointer motion"
#~ msgid "Minimum pressure:"
#~ msgstr "Presión mínima:"

#~ msgctxt "Pressure-dependent pointer motion"
#~ msgid "Minimum factor:"
#~ msgstr "Factor mínimo:"

#~ msgctxt "Pressure-dependent pointer motion"
#~ msgid "Maximum pressure:"
#~ msgstr "Presión máxima:"

#~ msgctxt "Pressure-dependent pointer motion"
#~ msgid "Maximum factor:"
#~ msgstr "Factor máximo:"

#~ msgid "Finger pressure at which minimum pressure motion factor is applied"
#~ msgstr "Presión co dedo a partir da cal se aplica o factor de movemento."

#~ msgid "Finger pressure at which maximum pressure motion factor is applied"
#~ msgstr "Presión co dedo ata a que se segue a aplicar o factor de movemento."

#~ msgid "Lowest setting for pressure motion factor"
#~ msgstr "Valor máis baixo do factor de movemento segundo a presión."

#~ msgid "Greatest setting for pressure motion factor"
#~ msgstr "Valor máis alto do factor de movemento segundo a presión."

#~ msgctxt "Pointer motion"
#~ msgid "Noise Cancellation"
#~ msgstr "Cancelación de ruído"

#~ msgctxt "Noise cancellation"
#~ msgid "Vertical:"
#~ msgstr "Vertical:"

#~ msgctxt "Noise cancellation"
#~ msgid "Horizontal:"
#~ msgstr "Horizontal:"

#~ msgid ""
#~ "The minimum vertical hardware distance required to generate motion events"
#~ msgstr ""
#~ "A distancia de soporte físico vertical mínima necesaria para xerar "
#~ "eventos de movemento."

#~ msgid " units"
#~ msgstr " unidades"

#~ msgid ""
#~ "The minimum horizontal hardware distance required to generate motion "
#~ "events"
#~ msgstr ""
#~ "A distancia de soporte físico horizontal mínima necesaria para xerar "
#~ "eventos de movemento."

#~ msgid "Scrolling"
#~ msgstr "Desprazamento"

#~ msgid "Edge scrolling:"
#~ msgstr "Desprazamento no bordo:"

#~ msgid "Enable vertical scrolling when dragging along the right edge"
#~ msgstr ""
#~ "Activar o desprazamento vertical ao pasar o dedo polo bordo dereito."

#~ msgctxt "Scrolling direction"
#~ msgid "Vertical"
#~ msgstr "Vertical."

#~ msgid "Enable horizontal scrolling when dragging along the bottom edge"
#~ msgstr ""
#~ "Activar o desprazamento horizontal ao pasar o dedo polo bordo inferior."

#~ msgctxt "Scrolling direction"
#~ msgid "Horizontal"
#~ msgstr "Horizontal."

#~ msgid ""
#~ "Enable vertical scrolling when dragging with two fingers anywhere on the "
#~ "touchpad"
#~ msgstr ""
#~ "Activar o desprazamento vertical ao pasar dous dedos ao mesmo tempo por "
#~ "calquera zona da área táctil."

#~ msgid ""
#~ "Enable horizontal scrolling when dragging with two fingers anywhere on "
#~ "the touchpad"
#~ msgstr ""
#~ "Activar o desprazamento horizontal ao pasar dous dedos ao mesmo tempo por "
#~ "calquera zona da área táctil."

#~ msgid "Reverse vertical scrolling"
#~ msgstr "Inverter o desprazamento vertical."

#~ msgid "Reverse horizontal scrolling"
#~ msgstr "Inverter o desprazamento horizontal."

#~ msgid "Two-finger scrolling:"
#~ msgstr "Desprazamento con dous dedos:"

#~ msgid "Reverse scrolling:"
#~ msgstr "Inverter o desprazamento:"

#~ msgctxt "Touchpad - Scrolling"
#~ msgid "Scrolling Distance"
#~ msgstr "Distancia de desprazamento"

#~ msgctxt "Scrolling distance"
#~ msgid "Vertical:"
#~ msgstr "Vertical:"

#~ msgid "Move distance of the finger for a scroll event"
#~ msgstr ""
#~ "Distancia de movemento do dedo para xerar un evento de desprazamento."

#~ msgid " mm"
#~ msgstr " mm"

#~ msgctxt "Scrolling distance"
#~ msgid "Horizontal:"
#~ msgstr "Horizontal:"

#~ msgid ""
#~ "Continue scrolling after the finger is released from the edge of the "
#~ "touchpad"
#~ msgstr ""
#~ "Continuar o desprazamento despois de que os dedos saian polo bordo da "
#~ "área táctil."

#~ msgctxt "Touchpad - Scrolling"
#~ msgid "Coasting"
#~ msgstr "Continuar o desprazamento."

#~ msgctxt "Coasting"
#~ msgid "Minimum speed:"
#~ msgstr "Velocidade mínima:"

#~ msgid ""
#~ "Your finger needs to produce this many scrolls per second in order to "
#~ "start coasting"
#~ msgstr ""
#~ "O dedo ten que producir este número de desprazamentos por segundo para "
#~ "que o desprazamento continúe cando o dedo abandone a área táctil polo "
#~ "bordo."

#~ msgid " scrolls/sec"
#~ msgstr " desprazamentos/s"

#~ msgctxt "Coasting"
#~ msgid "Deceleration:"
#~ msgstr "Desaceleración:"

#~ msgid "Number of scrolls/second² to decrease the coasting speed"
#~ msgstr ""
#~ "Número de desprazamentos/segundo² nos que reducir a velocidade de "
#~ "desprazamento cando o dedo abandona a área táctil polo bordo."

#~ msgid " scrolls/sec²"
#~ msgstr " desprazamentos/s²"

#~ msgid "Corner coasting:"
#~ msgstr "Continuar o desprazamento pola esquina:"

#~ msgid ""
#~ "Enable edge scrolling to continue while the finger stays in an edge corner"
#~ msgstr ""
#~ "Permitir que o desprazamento pola esquina continúe mentres o dedo se "
#~ "manteña na esquina."

#~ msgctxt "Coasting"
#~ msgid "Enable"
#~ msgstr "Activar."

#~ msgid ""
#~ "Scrolling is engaged when a drag starts in the given trigger region.\n"
#~ "Moving your finger in clockwise circles around the center of the "
#~ "touchpad\n"
#~ "will scroll down and counter clockwise motion will scroll up"
#~ msgstr ""
#~ "O desprazamento actívase cando comeza a arrastras na zona detonante.\n"
#~ "Ao debuxar círculos co dedo no sentido das agullas do reloxo ao redor do\n"
#~ "centro da área táctil, producirase desprazamento cara abaixo. No sentido\n"
#~ "oposto producirase desprazamento cara arriba."

#~ msgctxt "Touchpad - Scrolling"
#~ msgid "Circular Scrolling"
#~ msgstr "Desprazamento circular"

#~ msgctxt "Circular scrolling"
#~ msgid "Trigger region:"
#~ msgstr "Zona detonante:"

#~ msgid "Trigger region on the touchpad to start circular scrolling"
#~ msgstr ""
#~ "Zona detonante da área táctil para iniciar un desprazamento circular."

#~ msgctxt "Circular scrolling"
#~ msgid "Angle:"
#~ msgstr "Ángulo:"

#~ msgid "Move angle (degrees) of finger to generate a scroll event"
#~ msgstr ""
#~ "Ángulo de movemento (en graos) do dedo para xerar un evento de "
#~ "desprazamento."

#~ msgctxt "suffix - degrees (angle)"
#~ msgid "°"
#~ msgstr "°"

#~ msgid ""
#~ "When used together with vertical scrolling, hitting the upper or lower "
#~ "right corner will seamlessly switch over from vertical to circular "
#~ "scrolling"
#~ msgstr ""
#~ "Cando se combina co desprazamento vertical, ao tocar unha esquina dereita "
#~ "(superior ou inferior) o desprazamento pasará de vertical a circular."

#~ msgid "Sensitivity"
#~ msgstr "Sensibilidade"

#~ msgctxt "Touchpad"
#~ msgid "Sensitivity"
#~ msgstr "Sensibilidade"

#~ msgid "Pressure for detecting a touch:"
#~ msgstr "Presión para detectar un toque:"

#~ msgid "Pressure for detecting a release:"
#~ msgstr "Presión para detectar o remate dun toque:"

#~ msgid ""
#~ "When finger pressure goes above this value, the driver counts it as a "
#~ "touch"
#~ msgstr ""
#~ "Cando a presión que exerce un dedo supera este valor, o controlador "
#~ "interpreta que se trata dun toque."

#~ msgid ""
#~ "When finger pressure drops below this value, the driver counts it as a "
#~ "release"
#~ msgstr ""
#~ "Cando a presión que exerce un dedo cae por debaixo deste valor, o "
#~ "controlador interpreta que o toque rematou."

#~ msgid ""
#~ "If palm detection should be enabled. Note that this also requires "
#~ "hardware/firmware support from the touchpad"
#~ msgstr ""
#~ "Activar a detección da palma. Teña en conta que o soporte físico e o "
#~ "firmware da área táctil ten que ser compatíbel con esta funcionalidade."

#~ msgctxt "Touchpad"
#~ msgid "Palm Detection"
#~ msgstr "Detección da palma"

#~ msgctxt "Palm detection"
#~ msgid "Minimum width:"
#~ msgstr "Anchura mínima:"

#~ msgctxt "Palm detection"
#~ msgid "Minimum pressure:"
#~ msgstr "Presión mínima:"

#~ msgid "Minimum finger width at which touch is considered a palm"
#~ msgstr ""
#~ "Anchura mínima do dedo a partir da cal se considera que en realidade se "
#~ "trata da palma."

#~ msgid "Minimum finger pressure at which touch is considered a palm"
#~ msgstr ""
#~ "Presión mínima do dedo a partir da cal se considera que en realidade se "
#~ "trata da palma."

#~ msgctxt "Touchpad gesture"
#~ msgid "Taps"
#~ msgstr "Toques"

#~ msgid "Tap to Click"
#~ msgstr "Tocar para premer"

#~ msgctxt "Tapping"
#~ msgid "One finger:"
#~ msgstr "Un dedo:"

#~ msgid "Which mouse button is reported on a non-corner one-finger tap"
#~ msgstr ""
#~ "Botón do rato que se emula cando toca cun único dedo fóra das esquinas."

#~ msgctxt "Tapping"
#~ msgid "Two fingers:"
#~ msgstr "Dous dedos:"

#~ msgid "Which mouse button is reported on a non-corner two-finger tap"
#~ msgstr ""
#~ "Botón do rato que se emula cando toca con dous dedos fóra das esquinas."

#~ msgctxt "Tapping"
#~ msgid "Three fingers:"
#~ msgstr "Tres dedos:"

#~ msgid "Which mouse button is reported on a non-corner three-finger tap"
#~ msgstr ""
#~ "Botón do rato que se emula cando toca con tres dedos fóra das esquinas."

#~ msgctxt "Touchpad"
#~ msgid "Corners"
#~ msgstr "Esquinas"

#~ msgctxt "Touchpad corner"
#~ msgid "Top left:"
#~ msgstr "Superior esquerda:"

#~ msgid "Which mouse button is reported on a left top corner tap"
#~ msgstr "Botón do rato que se emula cando toca a esquina superior esquerda."

#~ msgctxt "Touchpad corner"
#~ msgid "Bottom left:"
#~ msgstr "Inferior esquerda:"

#~ msgctxt "Touchpad corner"
#~ msgid "Top right:"
#~ msgstr "Superior dereita:"

#~ msgid "Which mouse button is reported on a left bottom corner tap"
#~ msgstr "Botón do rato que se emula cando toca a esquina inferior esquerda."

#~ msgid "Which mouse button is reported on a right top corner tap"
#~ msgstr "Botón do rato que se emula cando toca a esquina superior dereita."

#~ msgid "Which mouse button is reported on a right bottom corner tap"
#~ msgstr "Botón do rato que se emula cando toca a esquina inferior dereita."

#~ msgctxt "Touchpad corner"
#~ msgid "Bottom right:"
#~ msgstr "Inferior dereita:"

#~ msgid ""
#~ "This gesture is an alternative way of dragging. It is performed by "
#~ "tapping (touching and releasing the finger), then touching again and "
#~ "moving the finger on the touchpad"
#~ msgstr ""
#~ "Este xesto é unha forma alternativa de arrastras. Realízase mediante un "
#~ "toque curto seguido dun toque con desprazamento."

#~ msgid "Tap-and-Drag Gesture"
#~ msgstr "Xesto de tocar e arrastrar"

#~ msgid ""
#~ "If off, a tap-and-drag gesture ends when you release the finger. If on, "
#~ "the gesture is active until you tap a second time, or until timeout "
#~ "expires"
#~ msgstr ""
#~ "Se está desactivado, o xesto de tocar e arrastrar remata en canto solta o "
#~ "dedo. Se está activado, o xesto continúa ata que volve facer un toque "
#~ "curto, ou ata que pasa o tempo límite."

#~ msgctxt "Touchpad gesture"
#~ msgid "Locked Drags"
#~ msgstr "Movementos de arrastrar bloqueados"

#~ msgctxt "Locked drags"
#~ msgid "Timeout:"
#~ msgstr "Tempo límite:"

#~ msgid ""
#~ "How long it takes (in milliseconds) for the \"Locked Drags\" mode to be "
#~ "automatically turned off after the finger is released from the touchpad"
#~ msgstr ""
#~ "Tempo (en milisegundos) necesario para que se bloqueen automaticamente os "
#~ "movementos de arrastrar despois de que o dedo abandone a área táctil."

#~ msgid "Tap Detection"
#~ msgstr "Detección de toques"

#~ msgid "Maximum time:"
#~ msgstr "Tempo máximo:"

#~ msgid "Maximum finger movement:"
#~ msgstr "Movemento máximo do dedo:"

#~ msgid "Maximum time for double tap:"
#~ msgstr "Tempo máximo para un toque duplo:"

#~ msgid "Single tap timeout:"
#~ msgstr "Tempo límite dun toque único:"

#~ msgid "Maximum time (in milliseconds) for detecting a tap"
#~ msgstr "Tempo máximo (en milisegundos) para detectar un toque."

#~ msgid "Maximum movement of the finger for detecting a tap"
#~ msgstr "Movemento máximo do dedo para detectar un toque."

#~ msgid "Maximum time (in milliseconds) for detecting a double tap"
#~ msgstr "Tempo máximo (en milisegundos) para detectar un toque duplo."

#~ msgid "Timeout after a tap to recognize it as a single tap"
#~ msgstr ""
#~ "Tempo límite despois dun toque para interpretalo como un toque único."

#~ msgid "Testing area"
#~ msgstr "Zona de probas"

#~ msgid "Click me"
#~ msgstr "Prémame"

#~ msgid "Acceleration profile:"
#~ msgstr "Perfil de aceleración:"

#~ msgid "Flat"
#~ msgstr "Plano"

#~ msgid "Adaptive"
#~ msgstr "Adaptábel"

#~ msgid "Touchpad KCM"
#~ msgstr "KCM de áreas táctiles"

#~ msgid "System Settings module for managing your touchpad"
#~ msgstr "Módulo de configuración do sistema para xestionar áreas táctiles."

#~ msgid "Copyright © 2016 Roman Gilg"
#~ msgstr "© 2016 Roman Gilg"

#~ msgid "Developer"
#~ msgstr "Desenvolvedor."

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"

#~ msgid ""
#~ "System Settings module, daemon and Plasma applet for managing your "
#~ "touchpad"
#~ msgstr ""
#~ "Módulo de configuración do sistema, servizo e trebello de Plasma para "
#~ "xestionar áreas táctiles."

#~ msgid "Copyright © 2013 Alexander Mezin"
#~ msgstr "© 2013 Alexander Mezin"

#~ msgid ""
#~ "This program incorporates work covered by this copyright notice:\n"
#~ "Copyright © 2002-2005,2007 Peter Osterlund"
#~ msgstr ""
#~ "Este programa inclúe traballo ao que corresponde a seguinte nota de "
#~ "autoría:\n"
#~ "© 2002-2005,2007 Peter Osterlund"

#~ msgid "Alexander Mezin"
#~ msgstr "Alexander Mezin"

#~ msgid "Thomas Pfeiffer"
#~ msgstr "Thomas Pfeiffer"

#~ msgctxt "Credits"
#~ msgid "Usability, testing"
#~ msgstr "Ergonomía, probas."

#~ msgid "Alex Fiestas"
#~ msgstr "Alex Fiestas"

#~ msgctxt "Credits"
#~ msgid "Helped a bit"
#~ msgstr "Botou unha man."

#~ msgid "Peter Osterlund"
#~ msgstr "Peter Osterlund"

#~ msgctxt "Credits"
#~ msgid "Developer of synclient"
#~ msgstr "Desenvolvedor de synclient."

#~ msgid "Vadim Zaytsev"
#~ msgstr "Vadim Zaytsev"

#~ msgctxt "Credits"
#~ msgid "Testing"
#~ msgstr "Probas."

#~ msgid "Violetta Raspryagayeva"
#~ msgstr "Violetta Raspryagayeva"

#~ msgid "Mouse Click Emulation"
#~ msgstr "Emulación do clic do rato"

#~ msgctxt "Synaptics touchpad driver"
#~ msgid "Synaptics backend not found"
#~ msgstr "Non se atopou a infraestrutura de Synaptics."

#~ msgid "Cannot read any of touchpad's properties"
#~ msgstr "Non é posíbel obter ningunha das propiedades do touchpad."

#~ msgid "Cannot read touchpad's capabilities"
#~ msgstr "Non é posíbel determinar as funcionalidades do touchpad."

#~ msgctxt "Libinput touchpad driver"
#~ msgid "Libinput backend not found"
#~ msgstr "Non se atopou a infraestrutura de Libinput."
