# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Dimitris Kardarakos <dimkard@gmail.com>, 2014, 2015, 2016, 2017.
# Dimitrios Glentadakis <dglent@free.fr>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-17 01:38+0000\n"
"PO-Revision-Date: 2017-01-15 17:59+0200\n"
"Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Χρησιμοποιείται αυτή τη στιγμή"

#: contents/activitymanager/ActivityItem.qml:245
#, fuzzy
#| msgid "Stop activity"
msgid ""
"Move to\n"
"this activity"
msgstr "Διακοπή δραστηριότητας"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Διαμόρφωση"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Διακοπή δραστηριότητας"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Σταματημένες δραστηριότητες:"

#: contents/activitymanager/ActivityManager.qml:120
#, fuzzy
#| msgid "Create activity..."
msgid "Create activity…"
msgstr "Δημιουργία δραστηριότητας..."

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Δραστηριότητες"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "Διαμόρφωση δραστηριότητας"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "Διαγραφή"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr ""

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr ""

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr ""

#: contents/applet/CompactApplet.qml:76
msgid "Open %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:21
#: contents/configuration/AppletConfiguration.qml:302
msgid "About"
msgstr ""

#: contents/configuration/AboutPlugin.qml:49
msgid "Send an email to %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:63
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:134
msgid "Copyright"
msgstr ""

#: contents/configuration/AboutPlugin.qml:152 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "Άδεια χρήσης:"

#: contents/configuration/AboutPlugin.qml:155
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr ""

#: contents/configuration/AboutPlugin.qml:169
#, fuzzy
#| msgid "Author:"
msgid "Authors"
msgstr "Συγγραφέας:"

#: contents/configuration/AboutPlugin.qml:180
msgid "Credits"
msgstr ""

#: contents/configuration/AboutPlugin.qml:192
msgid "Translators"
msgstr ""

#: contents/configuration/AboutPlugin.qml:209
msgid "Report a Bug…"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:74
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Keyboard Shortcuts"
msgstr "Συντομεύσεις πληκτρολογίου"

#: contents/configuration/AppletConfiguration.qml:350
msgid "Apply Settings"
msgstr "Εφαρμογή ρυθμίσεων"

#: contents/configuration/AppletConfiguration.qml:351
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Οι ρυθμίσεις του τρέχοντος αρθρώματος τροποποιήθηκαν. Επιθυμείτε την "
"εφαρμογή των τροποποιήσεων ή την απόρριψή τους;"

#: contents/configuration/AppletConfiguration.qml:382
msgid "OK"
msgstr "ΟΚ"

#: contents/configuration/AppletConfiguration.qml:390
msgid "Apply"
msgstr "Εφαρμογή"

#: contents/configuration/AppletConfiguration.qml:396
msgid "Cancel"
msgstr "Ακύρωση"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Αριστερό κουμπί"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Δεξί κουμπί"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Μεσαίο κουμπί"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Κουμπί-πίσω"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Κουμπί-εμπρός"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Κατακόρυφη κύλιση"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Οριζόντια κύλιση"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:98
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:170
msgctxt "@title"
msgid "About"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:185
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Προσθήκη ενέργειας"

#: contents/configuration/ConfigurationContainmentAppearance.qml:67
msgid "Layout changes have been restricted by the system administrator"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:82
msgid "Layout:"
msgstr "Διάταξη:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:96
#, fuzzy
#| msgid "Wallpaper Type:"
msgid "Wallpaper type:"
msgstr "Τύπος ταπετσαρίας:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:116
msgid "Get New Plugins…"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:184
msgid "Layout changes must be applied before other changes can be made"
msgstr "Οι αλλαγές στη διάταξη πρέπει να εφαρμοστούν πριν γίνουν άλλες αλλαγές"

#: contents/configuration/ConfigurationContainmentAppearance.qml:188
#, fuzzy
#| msgid "Apply now"
msgid "Apply Now"
msgstr "Εφαρμογή τώρα"

#: contents/configuration/ConfigurationShortcuts.qml:18
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Shortcuts"
msgstr "Συντομεύσεις πληκτρολογίου"

#: contents/configuration/ConfigurationShortcuts.qml:30
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""

#: contents/configuration/ContainmentConfiguration.qml:27
msgid "Wallpaper"
msgstr "Ταπετσαρία"

#: contents/configuration/ContainmentConfiguration.qml:32
msgid "Mouse Actions"
msgstr "Ενέργειες ποντικιού"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Είσοδος εδώ"

#: contents/configuration/PanelConfiguration.qml:91
#, fuzzy
#| msgid "Apply Settings"
msgid "Panel Settings"
msgstr "Εφαρμογή ρυθμίσεων"

#: contents/configuration/PanelConfiguration.qml:97
msgid "Add Spacer"
msgstr "Προσθήκη κενού"

#: contents/configuration/PanelConfiguration.qml:100
msgid "Add spacer widget to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:107
#, fuzzy
#| msgid "Add widgets"
msgid "Add Widgets…"
msgstr "Προσθήκη συστατικών"

#: contents/configuration/PanelConfiguration.qml:110
msgid "Open the widget selector to drag and drop widgets to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:132
msgid "Position"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:136
#: contents/configuration/PanelConfiguration.qml:285
msgid "Top"
msgstr "Πάνω"

#: contents/configuration/PanelConfiguration.qml:137
#: contents/configuration/PanelConfiguration.qml:287
msgid "Right"
msgstr "Δεξιά"

#: contents/configuration/PanelConfiguration.qml:138
#: contents/configuration/PanelConfiguration.qml:285
msgid "Left"
msgstr "Αριστερά"

#: contents/configuration/PanelConfiguration.qml:139
#: contents/configuration/PanelConfiguration.qml:287
msgid "Bottom"
msgstr "Κάτω"

#: contents/configuration/PanelConfiguration.qml:156
msgid "Set Position..."
msgstr ""

#: contents/configuration/PanelConfiguration.qml:221
#, fuzzy
#| msgid "Panel Alignment"
msgid "Alignment"
msgstr "Στοίχιση πίνακα"

#: contents/configuration/PanelConfiguration.qml:286
msgid "Center"
msgstr "Κέντρο"

#: contents/configuration/PanelConfiguration.qml:309
msgid "Length"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:327
#, fuzzy
#| msgid "Panel Alignment"
msgid "Fill height"
msgstr "Στοίχιση πίνακα"

#: contents/configuration/PanelConfiguration.qml:327
#, fuzzy
#| msgid "Uninstall widget"
msgid "Fill width"
msgstr "Απεγκατάσταση γραφικού συστατικού"

#: contents/configuration/PanelConfiguration.qml:328
msgid "Fit content"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:329
msgid "Custom"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:365
#, fuzzy
#| msgid "Visibility"
msgid "Visibility"
msgstr "Ορατότητα"

#: contents/configuration/PanelConfiguration.qml:377
#, fuzzy
#| msgid "Always Visible"
msgid "Always visible"
msgstr "Πάντα ορατό"

#: contents/configuration/PanelConfiguration.qml:378
#, fuzzy
#| msgid "Auto Hide"
msgid "Auto hide"
msgstr "Αυτόματη απόκρυψη"

#: contents/configuration/PanelConfiguration.qml:379
msgid "Dodge windows"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:417
msgid "Opacity"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:430
msgid "Adaptive"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:431
msgid "Opaque"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:432
#, fuzzy
#| msgid "Always Visible"
msgid "Translucent"
msgstr "Πάντα ορατό"

#: contents/configuration/PanelConfiguration.qml:456
msgid "Style"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:467
msgid "Floating"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:483
#, fuzzy
#| msgid "Panel Alignment"
msgid "Panel Width:"
msgstr "Στοίχιση πίνακα"

#: contents/configuration/PanelConfiguration.qml:483
#, fuzzy
#| msgid "Height"
msgid "Height:"
msgstr "Ύψος"

#: contents/configuration/PanelConfiguration.qml:517
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Focus shortcut:"
msgstr "Συντομεύσεις πληκτρολογίου"

#: contents/configuration/PanelConfiguration.qml:527
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:543
#, fuzzy
#| msgid "Remove Panel"
msgctxt "@action:button Delete the panel"
msgid "Delete Panel"
msgstr "Αφαίρεση πίνακα"

#: contents/configuration/PanelConfiguration.qml:546
msgid "Remove this panel; this action is undo-able"
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:30
#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:78
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Κλείσιμο"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
#, fuzzy
#| msgid "Remove Panel"
msgid "Remove Desktop"
msgstr "Αφαίρεση πίνακα"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "Αφαίρεση πίνακα"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr ""

#: contents/explorer/AppletAlternatives.qml:65
msgid "Alternative Widgets"
msgstr "Εναλλακτικά γραφικά συστατικά"

#: contents/explorer/AppletDelegate.qml:176
msgid "Undo uninstall"
msgstr "Αναίρεση απεγκατάστασης"

#: contents/explorer/AppletDelegate.qml:177
msgid "Uninstall widget"
msgstr "Απεγκατάσταση γραφικού συστατικού"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "Συγγραφέας:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "Email:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "Απεγκατάσταση"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
#, fuzzy
#| msgid "Widgets"
msgid "All Widgets"
msgstr "Γραφικά συστατικά"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Γραφικά συστατικά"

#: contents/explorer/WidgetExplorer.qml:152
#, fuzzy
#| msgid "Get new widgets"
msgid "Get New Widgets…"
msgstr "Λήψη νέων συστατικών"

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "Κατηγορίες"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets matched the search terms"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets available"
msgstr ""

#, fuzzy
#~| msgid "Maximize Panel"
#~ msgctxt "@action:button Make the panel as big as it can be"
#~ msgid "Maximize"
#~ msgstr "Μεγιστοποίηση πίνακα"

#, fuzzy
#~| msgid "Delete"
#~ msgctxt "@action:button Delete the panel"
#~ msgid "Delete"
#~ msgstr "Διαγραφή"

#, fuzzy
#~| msgid "More Settings..."
#~ msgid "More Options…"
#~ msgstr "Περισσότερες ρυθμίσεις..."

#~ msgid "Switch"
#~ msgstr "Εναλλαγή"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Above"
#~ msgstr "Τα παράθυρα επικαλύπτονται"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Below"
#~ msgstr "Τα παράθυρα επικαλύπτονται"

#, fuzzy
#~| msgid "Windows Can Cover"
#~ msgid "Windows In Front"
#~ msgstr "Τα παράθυρα τον επικαλύπτουν"

#, fuzzy
#~| msgid "Search..."
#~ msgid "Search…"
#~ msgstr "Αναζήτηση..."

#~ msgid "Screen Edge"
#~ msgstr "Άκρο οθόνης"

#~ msgid "Add Widgets..."
#~ msgstr "Προσθήκη συστατικών..."

#~ msgid "Width"
#~ msgstr "Πλάτος"

#, fuzzy
#~| msgid "Layout cannot be changed whilst widgets are locked"
#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr ""
#~ "Η διάταξη δεν μπορεί να τροποποιηθεί όταν τα γραφικά συστατικά είναι "
#~ "κλειδωμένα"

#~ msgid "Lock Widgets"
#~ msgstr "Κλείδωμα συστατικών"

#~ msgid ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."
#~ msgstr ""
#~ "Αυτή η συντόμευση θα ενεργοποιήσει αυτήν τη μικροεφαρμογή: η εστίαση του "
#~ "πληκτρολογίου θα δοθεί σε αυτή και αν η μικροεφαρμογή έχει αναδυόμενο "
#~ "(όπως το μενού εκκίνησης), το αναδυόμενο θα ανοίξει."

#~ msgid "Stop"
#~ msgstr "Διακοπή"

#~ msgid "Activity name:"
#~ msgstr "Όνομα δραστηριότητας:"

#~ msgid "Create"
#~ msgstr "Δημιουργία"

#~ msgid "Are you sure you want to delete this activity?"
#~ msgstr "Επιθυμείτε σίγουρα τη διαγραφή αυτής της δραστηριότητας;"

#, fuzzy
#~| msgid "Ok"
#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Ok"
#~ msgstr "Εντάξει"

#, fuzzy
#~| msgid "Apply"
#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Apply"
#~ msgstr "Εφαρμογή"

#, fuzzy
#~| msgid "Cancel"
#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Cancel"
#~ msgstr "Ακύρωση"

#~ msgid "Enter search term..."
#~ msgstr "Εισαγωγή όρου αναζήτησης..."

#~ msgid "Remove activity %1?"
#~ msgstr "Αφαίρεση δραστηριότητας %1;"

#~ msgid "Remove"
#~ msgstr "Αφαίρεση"

#~ msgid "Templates"
#~ msgstr "Πρότυπα"
