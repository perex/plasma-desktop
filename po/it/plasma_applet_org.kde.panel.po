# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# SPDX-FileCopyrightText: 2018, 2019, 2021, 2022, 2023 Vincenzo Reale <smart2128vr@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.panel\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-16 01:46+0000\n"
"PO-Revision-Date: 2023-11-14 14:09+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.3\n"

#: contents/ui/ConfigOverlay.qml:266 contents/ui/ConfigOverlay.qml:299
#, kde-format
msgid "Remove"
msgstr "Rimuovi"

#: contents/ui/ConfigOverlay.qml:276
#, kde-format
msgid "Configure…"
msgstr "Configura…"

#: contents/ui/ConfigOverlay.qml:286
#, kde-format
msgid "Show Alternatives…"
msgstr "Mostra alternative..."

#: contents/ui/ConfigOverlay.qml:309
#, kde-format
msgid "Spacer width"
msgstr "Larghezza dello spaziatore"

#: contents/ui/main.qml:428
#, kde-format
msgid "Add Widgets…"
msgstr "Aggiungi oggetti..."
