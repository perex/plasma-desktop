# translation of plasma_applet_org.kde.panel.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2018.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.panel\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-16 01:46+0000\n"
"PO-Revision-Date: 2022-05-16 18:20+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.1\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: contents/ui/ConfigOverlay.qml:266 contents/ui/ConfigOverlay.qml:299
#, kde-format
msgid "Remove"
msgstr "Odstrániť"

#: contents/ui/ConfigOverlay.qml:276
#, kde-format
msgid "Configure…"
msgstr "Nastaviť..."

#: contents/ui/ConfigOverlay.qml:286
#, kde-format
msgid "Show Alternatives…"
msgstr "Zobraziť alternatívy..."

#: contents/ui/ConfigOverlay.qml:309
#, kde-format
msgid "Spacer width"
msgstr "Šírka medzery"

#: contents/ui/main.qml:428
#, kde-format
msgid "Add Widgets…"
msgstr ""
