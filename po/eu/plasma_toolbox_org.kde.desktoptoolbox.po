# Translation for plasma_toolbox_org.kde.desktoptoolbox.po to Euskara/Basque (eu).
# Copyright (C) 2018-2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2018, 2019, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-11 02:07+0000\n"
"PO-Revision-Date: 2023-01-06 20:14+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.1\n"

#: contents/ui/ToolBoxContent.qml:266
#, kde-format
msgid "Choose Global Theme…"
msgstr "Hautatu gai orokorra..."

#: contents/ui/ToolBoxContent.qml:273
#, kde-format
msgid "Configure Display Settings…"
msgstr "Konfiguratu bistaratzeko ezarpenak..."

#: contents/ui/ToolBoxContent.qml:294
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "Gehiago"

#: contents/ui/ToolBoxContent.qml:308
#, kde-format
msgid "Exit Edit Mode"
msgstr "Irten editatzeko modutik"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Amaitu antolamendua norbere erara egokitzea"

#~ msgid "Default"
#~ msgstr "Lehenetsia"

#~ msgid "Desktop Toolbox"
#~ msgstr "Mahaigaineko tresna-kutxa"

#~ msgid "Desktop Toolbox — %1 Activity"
#~ msgstr "Mahaigaineko tresna-kutxa — %1 jarduera"

#~ msgid "Lock Screen"
#~ msgstr "Giltzatu pantaila"

#~ msgid "Leave"
#~ msgstr "Irten"
