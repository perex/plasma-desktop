# Translation of kcm_workspace.po to Brazilian Portuguese
# Copyright (C) 2014-2020 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2014, 2019, 2020.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2017, 2018, 2019, 2020, 2022, 2023.
# Thiago Masato Costa Sueto <herzenschein@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-04 12:20+0000\n"
"PO-Revision-Date: 2023-11-14 14:03-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: ui/main.qml:24
#, kde-format
msgid ""
"The system must be restarted before changes to the middle-click paste "
"setting can take effect."
msgstr ""
"O sistema deve ser reiniciado antes que as alterações de colar com o botão "
"do meio tenham efeito."

#: ui/main.qml:30
#, kde-format
msgid "Restart"
msgstr "Reiniciar"

#: ui/main.qml:47
#, kde-format
msgid "Visual behavior:"
msgstr "Comportamento do visual:"

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: ui/main.qml:48 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr "Exibir dicas informativas ao passar com o mouse"

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: ui/main.qml:59 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr "Exibir retorno visual para alterações de status"

#: ui/main.qml:77
#, kde-format
msgid "Animation speed:"
msgstr "Velocidade da animação:"

#: ui/main.qml:103
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Lenta"

#: ui/main.qml:109
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Instantânea"

#: ui/main.qml:123
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Um clique em arquivos e pastas:"

#: ui/main.qml:130
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Faz com que selecione"

#: ui/main.qml:144
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Para abrir, use clique duplo"

#: ui/main.qml:156
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "Faz com que abra"

#: ui/main.qml:169
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Para selecionar, clique no marcador de seleção (+)"

#: ui/main.qml:184
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr "Clicar na barra de rolagem:"

#: ui/main.qml:185
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr "Desloca para a posição clicada"

#: ui/main.qml:203
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr "Desloca uma página para cima ou para baixo"

#: ui/main.qml:216
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr "Para rolar até a posição clicada, use o botão do meio"

#: ui/main.qml:229
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Middle click pastes selected text'"
msgid "Middle Click:"
msgstr "Clique do meio:"

#: ui/main.qml:231
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Middle click pastes selected text'"
msgid "Pastes selected text"
msgstr "Cola o texto selecionado"

#: ui/main.qml:248
#, kde-format
msgid "Touch Mode:"
msgstr "Modo de toque:"

#: ui/main.qml:250
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr "Habilitar automaticamente quando necessário"

#: ui/main.qml:250 ui/main.qml:293
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr "Nunca habilitar"

#: ui/main.qml:266
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""
"O modo de toque será automaticamente ativado sempre que o sistema detectar "
"uma tela sensível ao toque mas nenhum mouse ou touchpad. Por exemplo, quando "
"o teclado de um laptop conversível é virado ou desconectado."

#: ui/main.qml:271
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr "Sempre habilitado"

#: ui/main.qml:311
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""
"No modo de toque muitos elementos da interface se tornarão maiores para "
"acomodar mais facilmente as interações com os toques."

#: ui/main.qml:328
#, kde-format
msgid "Double-click interval:"
msgstr "Intervalo do clique duplo:"

#: ui/main.qml:343
#, kde-format
msgid "%1 msec"
msgid_plural "%1 msec"
msgstr[0] "%1 ms"
msgstr[1] "%1 ms"

#: ui/main.qml:347
#, kde-format
msgid "msec"
msgstr "ms"

#: ui/main.qml:361
#, kde-format
msgid ""
"Two clicks within this duration are considered a double-click. Some "
"applications may not honor this setting."
msgstr ""

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Single click to open files"
msgstr "Clique único para abrir arquivos"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, kde-format
msgid "Animation speed"
msgstr "Velocidade da animação"

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr ""
"Clique com o botão esquerdo na barra de rolagem move a barra de rolagem em "
"uma página"

#. i18n: ectx: label, entry (doubleClickInterval), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:21
#, kde-format
msgid "Double click interval"
msgstr "Intervalo do clique duplo"

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr "Alterar automaticamente para o modo otimizado para o toque"

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr "Habilitar a colagem de seleção com o botão do meio"

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr ""
"Exibir uma visualização na tela (OSD) indicando mudanças de estado como "
"brilho ou volume"

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr "OSD ao alterar o layout"

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr "Exibir um popup ao alterar o layout"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "André Marcelo Alvarenga, Luiz Fernando Ranghetti"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "alvarenga@kde.org, elchevive@opensuse.org"

#~ msgid "General Behavior"
#~ msgstr "Comportamento geral"

#~ msgid "System Settings module for configuring general workspace behavior."
#~ msgstr ""
#~ "Módulo das configurações do sistema para configurar o comportamento geral "
#~ "do espaço de trabalho."

#~ msgid "Furkan Tokac"
#~ msgstr "Furkan Tokac"

#~ msgid "Click behavior:"
#~ msgstr "Comportamento do clique:"

#~ msgid "Double-click to open files and folders"
#~ msgstr "Clique duplo para abrir arquivos e pastas"

#~ msgid "Select by single-clicking"
#~ msgstr "Selecionar com clique simples"
